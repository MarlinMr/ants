var worldWidth = 1280;
var worldHeight = 720;
var floorColor = '#007f00';
var tick = 0;
var ants = [];
var prey = [];
var preyColor = '#0000ff';
var foods = [];
var barrier = [];
var smellDistance = 50;
var foodStartSize = 50;

function onLoad(){
    // Define Canvas
    canvas          = document.getElementById("GUICanvas");
    canvas.width    = worldWidth;
    canvas.height   = worldHeight;
    ctx             = canvas.getContext("2d");
    ctx.font = "30px Arial";
    // Manually place food
    canvas.addEventListener("mousedown", function(event){
        var rect = canvas.getBoundingClientRect();
        var ratiox = rect.width/worldWidth;
        var ratioy = rect.height/worldHeight;
        var x = event.x/ratiox;
        var y = event.y/ratioy;
        foods[foods.length] = {x:x,y:y,size:foodStartSize};
        }, false);
    canvas.addEventListener("contextmenu", function(event) {
        var rect = canvas.getBoundingClientRect();
        var ratiox = rect.width/worldWidth;
        var ratioy = rect.height/worldHeight;
        var x = event.x/ratiox;
        var y = event.y/ratioy;
        barrier[barrier.length] = {x:x,y:y};
      }, false);
    genAnts();
    // Start Game
    window.requestAnimationFrame(gameLoop);
}

function gameLoop(){
    if (!document.getElementById("pause").checked){
       // genAnts();
        tick += 1;
        // Paint Floor
        ctx.fillStyle = floorColor;
        ctx.fillRect(0,0,canvas.width,canvas.height);
        // Paint AntHill
        drawCircle({x:worldWidth/2, y:worldHeight/2}, 30, '#545151');
        drawCircle({x:worldWidth/2, y:worldHeight/2}, 5, '#242121');
        drawPrey();
        drawFoods();
        drawBarrier();
        drawAnts();
        calcTurn();
        ctx.fillStyle = "black";
        ctx.fillText(("number of ants: " + ants.length), 10, 50);
        ctx.fillText(("number of prey: " + prey.length), 10, 75);
        ctx.fillText(("number of food: " + foods.length), 10, 100);
    }
    window.requestAnimationFrame(gameLoop);
}

function drawAnts(){
    for (i=0; i<ants.length; i++){
        drawCircle(ants[i], 2, '#000000');
        //ctx.font = "10px Arial";
        //ctx.fillText(i, ants[i].x, ants[i].y);
        if (ants[i].hasFood){drawCircle(ants[i], 1, '#ffff00');
        }else {drawCircle(ants[i], 1, '#ff0000');}
    }
}
function drawPrey(){
    for (i=0; i<prey.length; i++){
        //drawCircle(prey[i], smellDistance, '#000000');
        drawCircle(prey[i], 1, '#0000ff');
    }
}
function drawFoods(){
    for (i=0; i<foods.length; i++){
        //drawCircle(foods[i], smellDistance, '#000000');
        drawCircle(foods[i], 1, '#00FFff');
    }
}
function drawBarrier(){
    for (i=0; i<barrier.length; i++){
        drawCircle(barrier[i], 15, '#000000');
    }
}

function calcTurn() {
//    everyOtherAnt=Math.floor(Math.random()*10)+1;
//    console.log(everyOtherAnt);
    for (i=0; i<ants.length; i=Math.floor(Math.random()*ants.length)+1){
        if (ants[i].seesFood||ants[i].hasFood){a=0;}
        else {a=Math.random();}
        if (a<0.1){
            //ants[i].x = ants[i].x+(Math.random()-0.5)*10;
            //ants[i].y = ants[i].y+(Math.random()-0.5)*10;
            ants[i].x = ants[i].x+Math.cos(ants[i].direction); 
            ants[i].y = ants[i].y+Math.sin(ants[i].direction); 
            ants[i].x = outsideBorderX(ants[i].x);
            ants[i].y = outsideBorderY(ants[i].y);
            if (!ants[i].hasFood) {
            for (j=0; j<prey.length; j++){
                if (absoluteDistance(ants[i],prey[j])<smellDistance){
                    if (absoluteDistance(ants[i],prey[j])<2){
                        ants[i].hasFood = true;
                        ants[i].seesFood = false;
                        prey.splice(j,1);
                        break;
                    }
                    ants[i].seesFood = true;
                    ants[i].direction = Math.atan2(prey[j].y - ants[i].y, prey[j].x - ants[i].x);
                    break;
                } else {ants[i].seesFood = false;}
            }
            for (j=0; j<foods.length; j++){
                if (absoluteDistance(ants[i],foods[j])<smellDistance){
                    if (absoluteDistance(ants[i],foods[j])<2){
                        ants[i].hasFood = true;
                        ants[i].seesFood = false;
                        foods.splice(j,1);
                        break;
                    }
                    ants[i].seesFood = true;
                    ants[i].direction = Math.atan2(foods[j].y - ants[i].y, foods[j].x - ants[i].x);
                    break;
                } else {ants[i].seesFood = false;}
            }
            } else {
                        ants[i].direction = Math.atan2(worldHeight/2 - ants[i].y, worldWidth/2 - ants[i].x);
                        if (absoluteDistance(ants[i],{x:worldWidth/2,y:worldHeight/2})<1){ants[i].hasFood=false; genAnts();}
                    }
            if (Math.random()<0.01){ants[i].direction = ants[i].direction+(Math.random()-0.5);}
        }
    }
    for (i=0; i<prey.length; i++){
       prey[i].x = prey[i].x+Math.cos(prey[i].direction); 
       prey[i].y = prey[i].y+Math.sin(prey[i].direction); 
       prey[i].x = outsideBorderX(prey[i].x);
       prey[i].y = outsideBorderY(prey[i].y);
       if (Math.random()>0.2){prey[i].direction = prey[i].direction+(Math.random()-0.5);}
    }
    if (Math.random()<0.01){
        genPrey();
    }
}

function outsideBorderX(x){
    if (x < 0){x = worldWidth-x;}
    else if (x > worldWidth){x = x-worldWidth;}
    return x;
}
function outsideBorderY(y){
    if (y < 0){y = worldHeight-y;}
    else if (y > worldHeight){y = y-worldHeight;}
    return y;
}
function absoluteDistance(point1,point2){
    xdist = point1.x - point2.x;
    ydist = point1.y - point2.y;
    return Math.sqrt((xdist*xdist) + (ydist*ydist));
}
function genAnts(){
    ants[ants.length] = { 
        x:worldWidth/2,
        y:worldHeight/2,
        direction:Math.random()*2*Math.PI,
        hasFood:false,
        seesFood:false
    }
}

function genPrey(){
    prey[prey.length] = { 
        x:0,
        y:Math.floor(Math.random()*worldHeight),
        direction:Math.random()*2*Math.PI
    }
}

function drawCircle(point, radius, color){
    ctx.beginPath();
    ctx.arc(point.x, point.y, radius, 0, Math.PI*2, true);
    ctx.fillStyle = color;
    ctx.fill();
}
